# Windows Ansible Playbook

An Ansible playbook to configure a Windows server.

## Pre-requisites

Ansible must be installed to run the playbook.

## Configure

Create a file named `hosts.ini` at the root of the project following the example in `hosts.example.ini` with the host IP and Windows password for the Administrator user.


## Password

Here is an example of how to get the Windows password from an AWS EC2 Windows server:

``` bash
aws ec2 get-password-data \
  --instance-id "${INSTANCE_ID}" \
  --priv-launch-key "${KEY_NAME}.pem" \
  --query "PasswordData" \
  --output text
```

(Assuming that the aws-cli is installed, and INSTANCE_ID and KEY_NAME are set.)

## Run

`ansible-playbook windows.playbook.yml`

## Author

**Andre Silva** - [andreswebs](https://gitlab.com/andreswebs)
